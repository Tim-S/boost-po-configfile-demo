#pragma once

#include <boost/algorithm/string.hpp>

#include "programm_options_parse_children.hpp"

// #define ENABLE_YAML_CONFIG_FILES
#ifdef ENABLE_YAML_CONFIG_FILES
#ifdef ENABLE_LIBYAML
#include <yaml.h>
#endif
#ifdef ENABLE_YAMLCPP
#include <yaml-cpp/yaml.h>
#endif
#endif

/**
 * Read YAML from a the given stream and translate it to a property tree.
 * @throw runtime_error In case of error deserializing the property
 *                          tree.
 * @param stream Stream from which to read in the property tree.
 * @param[out] pt The property tree to populate.
 */
template <class Ptree>
void read_yaml(std::basic_istream<
                   typename Ptree::key_type::value_type> &stream,
               Ptree &pt)
{
  std::string s(std::istreambuf_iterator<typename Ptree::key_type::value_type>(stream), {}); // read whole stream into string
  #ifdef ENABLE_YAMLCPP
  std::list<std::string> keyNameList;
  Ptree children;

  YAML::Node node = YAML::Load(s);
  for(YAML::const_iterator it=node.begin();it!=node.end();++it) {
    switch (node.Type())
    {
    case YAML::NodeType::Map:
      {
        auto&& key = (*it).first.as<std::string>(); 
        keyNameList.emplace_back(key);
        auto& val =(*it).second;

        if(val.IsSequence())
        {
          children = Ptree();
          for (YAML::const_iterator it = val.begin(); it != val.end(); it++)
          {
            Ptree child;
            child.put("", (*it).Scalar());
            children.push_back(std::make_pair("", child));
          }
          std::string ptreeKey = boost::algorithm::join(keyNameList, ".");
          pt.add_child(ptreeKey, children);
          keyNameList.pop_back();
        }else
        if(val.IsScalar()){
          std::string ptreeKey = boost::algorithm::join(keyNameList, ".");
          pt.put(ptreeKey, val.Scalar());
          keyNameList.pop_back();
        }
      }
      break;
    case YAML::NodeType::Sequence:
      break;
    case YAML::NodeType::Scalar:
      break;
    default:
      break;
    }
  }
  #endif

  #ifdef ENABLE_LIBYAML
  // libyaml specific
  yaml_parser_t parser;
  yaml_event_t event;

  // initialize parsing
  yaml_parser_initialize(&parser);
  yaml_parser_set_input_string(&parser, (yaml_char_t *)s.c_str(), s.length());

  bool done = false;
  bool map_key_is_next = false;
  std::list<std::string> keyNameList;
  Ptree children;
  bool array_elem_is_next = false;

  while (!done)
  {
    if (!yaml_parser_parse(&parser, &event))
    {
      throw std::runtime_error("some sort of error happened");
    }

    switch (event.type)
    {
    case YAML_NO_EVENT:
      break;
    case YAML_MAPPING_START_EVENT:
      map_key_is_next = true;
      break;
    case YAML_MAPPING_END_EVENT:
      if (!keyNameList.empty())
      {
        keyNameList.pop_back();
      }
      break;
    case YAML_SCALAR_EVENT:
      if (map_key_is_next)
      {
        keyNameList.emplace_back((char *)event.data.scalar.value);
        map_key_is_next = false;
      }
      else if(array_elem_is_next){
        Ptree child;
        child.put("", std::string((char *)event.data.scalar.value));
        children.push_back(std::make_pair("", child));
      }
      else
      {
        map_key_is_next = true;
        std::string ptreeKey = boost::algorithm::join(keyNameList, ".");
        std::string value = (char *)event.data.scalar.value;
        pt.put(ptreeKey, value);

        keyNameList.pop_back();
      }
      break;
    case YAML_SEQUENCE_START_EVENT:
      {
        children = Ptree();
        array_elem_is_next = true;
      }
      break;
    case YAML_SEQUENCE_END_EVENT:
      {
        std::string ptreeKey = boost::algorithm::join(keyNameList, ".");
        pt.add_child(ptreeKey, children);
        array_elem_is_next = false;  
        { // assuming next event is a new map_key // i.e. i.e. nested arrays not supported for now
          map_key_is_next = true;
          keyNameList.pop_back();
        }
      }
      break;
    default:
      break;
    }
    done = (event.type == YAML_STREAM_END_EVENT);
    yaml_event_delete(&event);
  }

  yaml_parser_delete(&parser);
  #endif
}

template <class charT>
void parse_yaml_options(std::basic_istream<charT>& is, boost::program_options::parsed_options& options)
{
  boost::property_tree::basic_ptree<std::basic_string<charT>, std::basic_string<charT>> pt;
  read_yaml(is, pt);
  parse_children<charT>(std::basic_string<charT>(), pt, options);
}

template <class charT>
boost::program_options::basic_parsed_options<charT> parse_yaml_config_file(std::basic_istream<charT>& is, const boost::program_options::options_description& desc, bool allow_unregistered = false)
{     
  // do any option check here
  boost::program_options::parsed_options result(&desc);
  parse_yaml_options(is, result);
  return boost::program_options::basic_parsed_options<charT>(result);
}
