#pragma once

#include <string>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>

template <class charT>
void parse_children(std::string const& prefix, boost::property_tree::ptree& tree,
                   boost::program_options::parsed_options& options) {
  if (tree.empty()) {
    // remove first dot
    std::basic_string<charT> name = prefix.substr(1);
    // remove last dot if present
    if (name.back() == '.') {
      name.pop_back();
    }
    std::basic_string<charT> value = tree.data();

    boost::program_options::basic_option<charT> opt;
    opt.string_key = name;
    opt.value.push_back(value);
    opt.unregistered = (options.description->find_nothrow(name, false) == nullptr);
    opt.position_key = -1;
    // append value to existing option-key if it exists
    for (auto& o : options.options) {
      if (o.string_key == name) {
        o.value.push_back(value);
        return;
      }
    }
    options.options.push_back(opt);
  } else {
    for (auto it = tree.begin(); it != tree.end(); ++it) {
      parse_children<charT>(prefix + "." + it->first, it->second, options);
    }
  }
}