#include <boost/program_options.hpp>
#include <boost/program_options/cmdline.hpp>
// #include <boost/program_options/config.hpp>

#include <boost/filesystem.hpp>

#include <string>
#include <iostream>
#include <fstream>
namespace po = boost::program_options;
namespace fs = boost::filesystem;

#include "program_options_yaml.hpp"
#include "program_options_json.hpp"

template <typename TElem>
bool try_print_vec(const boost::program_options::variables_map vm, po::variables_map::const_iterator it, std::ostream& os){
  try
  {
    boost::any_cast<std::vector<TElem>>(it->second.value());
  }
  catch (const boost::bad_any_cast &)
  {
    return false;
  }
  
  try
  {
    std::vector<TElem> vect = vm[it->first].as<std::vector<TElem>>();
    size_t i = 0;
    for (std::vector<TElem>::iterator oit = vect.begin();
          oit != vect.end(); oit++, ++i)
    {
      os << "\r> " << it->first << "[" << i << "]=" << (*oit) << std::endl;
    }
    return true;
  }
  catch (const boost::bad_any_cast &)
  {
    os << "UnknownType(" << ((boost::any)it->second.value()).type().name() << ")" << std::endl;
    return false;
  }
}

std::ostream& print_variablemap(const boost::program_options::variables_map vm, std::ostream& os)
{
  for (po::variables_map::const_iterator it = vm.begin(); it != vm.end(); it++)
  {
    os << "> " << it->first;
    if (((boost::any)it->second.value()).empty())
    {
      os << "(empty)";
    }
    if (vm[it->first].defaulted() || it->second.defaulted())
    {
      os << "(default)";
    }
    os << "=";

    bool is_char;
    try
    {
      boost::any_cast<const char *>(it->second.value());
      is_char = true;
    }
    catch (const boost::bad_any_cast &)
    {
      is_char = false;
    }
    bool is_str;
    try
    {
      boost::any_cast<std::string>(it->second.value());
      is_str = true;
    }
    catch (const boost::bad_any_cast &)
    {
      is_str = false;
    }

    if (((boost::any)it->second.value()).type() == typeid(int))
    {
      os << vm[it->first].as<int>() << std::endl;
    }
    else if (((boost::any)it->second.value()).type() == typeid(bool))
    {
      os << vm[it->first].as<bool>() << std::endl;
    }
    else if (((boost::any)it->second.value()).type() == typeid(double))
    {
      os << vm[it->first].as<double>() << std::endl;
    }
    else if (((boost::any)it->second.value()).type() == typeid(float))
    {
      os << vm[it->first].as<float>() << std::endl;
    }
    else if (is_char)
    {
      os << vm[it->first].as<const char *>() << std::endl;
    }
    else if (is_str)
    {
      std::string temp = vm[it->first].as<std::string>();
      if (temp.size())
      {
        os << temp << std::endl;
      }
      else
      {
        os << "true" << std::endl;
      }
    }
    else if(try_print_vec<float>(vm, it, os)){}
    else if(try_print_vec<std::string>(vm, it, os)){}
    else 
    { 
      os << "UnknownType(" << ((boost::any)it->second.value()).type().name() << ")" << std::endl;
    }
  }
  return os;
}

int main(int argc, char** argv) {
  // Arguments will be stored here
  std::string input;
  std::string output;
  std::string loglevel;
  std::vector<std::string> config_files;
  int someint;
  std::vector<float> somefloats;
  std::vector<std::string> somestrings;

  // Parse command line arguments

  // Configure options here
  po::options_description generic("Generic options");
  generic.add_options()
      ("config-files,c", po::value<decltype(config_files)>()->multitoken(), "Configuration files to get settings from (NOTE: settings from previous files take precedence over following files)");

  // Declare a group of options that will be allowed both on command line and in config file
  po::options_description main_options ("Main options");
  main_options.add_options ()
      ("help,h", "print usage message")
      ("loglevel", po::value(&loglevel)->default_value("info"), "loglevel")
      ("input,i", po::value(&input)->required(), "Input file")
      ("someint", po::value(&someint)->required(), "some int")
      ("somestrings", po::value(&somestrings)->multitoken(), "some strings")
      ("somefloats", po::value(&somefloats)->multitoken(), "some floats");

  // set options allowed on command line
  po::options_description cmdline_options;
  cmdline_options.add(generic).add(main_options);

  // set options allowed in config file
  po::options_description config_file_options;
  config_file_options.add(main_options);

  // set options shown by --help
  po::options_description visible("Allowed options");
  visible.add(generic).add(main_options);
  
  po::variables_map vm;
  try{
    // parse commandline-options first
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
    
    // Check if --help is given
    if (vm.count ("help")) {
        std::cerr << visible << "\n";
        return EXIT_FAILURE;
    }

    // try to fill remaining options from config-files
    if(vm.count ("config-files") ){
      const bool allow_unregistered = true; // allow uregistered options to avoid backward-incompatible options in config-files
      for(auto file : vm.at("config-files").as<decltype(config_files)>()) try {
        std::ifstream ifs(file, std::ifstream::in);
        if (!ifs) std::cerr << "can not open configuration file: " << file << "\n";
        // select config-parser based on file-extension
        std::string extension = boost::filesystem::extension(file);
        if(extension == ".json"){
          po::store(parse_json_config_file(ifs, config_file_options, allow_unregistered), vm);
#ifdef ENABLE_YAML_CONFIG_FILES
        }else if(extension == ".yaml" || extension == ".yml"){
          po::store(parse_yaml_config_file(ifs, config_file_options, allow_unregistered), vm);
#endif
        }else{ // assume ini-style config
          po::store(parse_config_file(ifs, config_file_options, allow_unregistered), vm);
        }
      } catch(std::exception const& e) {
        std::cerr << file << ": " << e.what() << "\n";
      }
    }
    po::notify (vm);

    std::cout << "Configuration: \n";
    print_variablemap(vm, std::cout);
  }catch(std::exception const& e) {
    std::cerr << e.what() << "\n";
    std::cerr << visible << "\n";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}