
#pragma once

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "programm_options_parse_children.hpp"

template <class charT>
void parse_json_options(std::basic_istream<charT>& is, boost::program_options::parsed_options& options)
{
  boost::property_tree::basic_ptree<std::basic_string<charT>, std::basic_string<charT>> pt;
  boost::property_tree::read_json(is, pt);

  parse_children<charT>(std::basic_string<charT>(), pt, options);
}

template <class charT>
boost::program_options::basic_parsed_options<charT> parse_json_config_file(std::basic_istream<charT>& is, const boost::program_options::options_description& desc, bool allow_unregistered = false)
{     
  // do any option check here
  boost::program_options::parsed_options result(&desc);
  parse_json_options(is, result);
  return boost::program_options::basic_parsed_options<charT>(result);
}
