cmake_minimum_required(VERSION 3.14...3.27)
include(CMakeDependentOption)

option(ENABLE_LIBYAML "enable YAML config files using libyaml" OFF)
cmake_dependent_option(ENABLE_YAMLCPP "enable YAML config files using yaml-cpp" OFF "ENABLE_LIBYAML" ON)
cmake_dependent_option(ENABLE_YAML_CONFIG_FILES "enable YAML config files" ON "ENABLE_LIBYAML OR ENABLE_YAMLCPP" OFF)

project( boost-po-configfile-demo VERSION 1.0 LANGUAGES CXX )
  
set(CMAKE_CXX_STANDARD 17)

# cmake -B build -DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreaded
set(CMAKE_MSVC_RUNTIME_LIBRARY  "MultiThreaded$<$<CONFIG:Debug>:Debug>")

set(Boost_USE_STATIC_LIBS      ON)
set(Boost_USE_MULTITHREADED    ON)
set(Boost_USE_STATIC_RUNTIME   ON)
# set(Boost_USE_DEBUG_LIBS     OFF)
# set(Boost_DEBUG ON)
find_package(Boost COMPONENTS filesystem program_options iostreams REQUIRED)

include(FetchContent)
Set(FETCHCONTENT_QUIET FALSE)

add_executable(configfiles 
  src/configfiles.cpp
)
target_link_libraries(configfiles 
  PUBLIC Boost::headers Boost::filesystem Boost::program_options
)
add_test(NAME test_config_ini COMMAND configfiles -c ${CMAKE_SOURCE_DIR}/config/config.cfg)
add_test(NAME test_config_json COMMAND configfiles -c ${CMAKE_SOURCE_DIR}/config/config.json)

if(ENABLE_YAML_CONFIG_FILES)
  target_compile_definitions(configfiles PUBLIC ENABLE_YAML_CONFIG_FILES)

  if(ENABLE_LIBYAML)
    FetchContent_Declare(
      libyaml
      GIT_REPOSITORY "https://github.com/yaml/libyaml.git"
      GIT_TAG 0.2.5
      GIT_PROGRESS TRUE
    )
    FetchContent_MakeAvailable(libyaml)

    target_compile_definitions(configfiles PUBLIC ENABLE_LIBYAML)
    target_link_libraries(configfiles PUBLIC yaml)
  endif()

  if(ENABLE_YAMLCPP)
    FetchContent_Declare(
      yaml-cpp
      GIT_REPOSITORY "https://github.com/jbeder/yaml-cpp.git"
      GIT_TAG yaml-cpp-0.7.0
      GIT_PROGRESS TRUE
    )
    FetchContent_MakeAvailable(yaml-cpp)

    target_compile_definitions(configfiles PUBLIC ENABLE_YAMLCPP)
    target_link_libraries(configfiles PUBLIC yaml-cpp)
  endif()

  add_test(NAME test_config_yaml COMMAND configfiles -c ${CMAKE_SOURCE_DIR}/config/config.yaml)
endif()

include(CTest)